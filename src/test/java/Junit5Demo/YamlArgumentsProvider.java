package Junit5Demo;

import java.io.File;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.support.AnnotationConsumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class YamlArgumentsProvider implements ArgumentsProvider, AnnotationConsumer<YamlSource> {
    private String path;
    private Class<?> clazz;
   
    YamlArgumentsProvider() {
    }
    
    @Override
    public void accept(YamlSource source) {
        path = source.path();
        clazz = source.clazz();
    }
    
    public Stream provideArguments(ExtensionContext context) {
    	ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
		
		Object obj = null;
		try {
			obj = mapper.readValue(new File(path), clazz);
		} catch (Throwable e) {
			e.printStackTrace();
		}
        
		return Stream.of(Arguments.of(obj));
    }
}
